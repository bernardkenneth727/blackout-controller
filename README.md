# Blackout Controller
- Blackout controller is a space simulation consisting of satellites and devices where files are sent from devices to satellites and to other devices/satellitesa
- The simulation incorporates use of Java and object oriented design to constrcut the backend for the simulation 
- A detailed ERD is available in design.pdf 


# Instructions: 

Step 1: 
- Go to src/main/App.java and click Run in the App.java file 
<img src="step1.png">



Step 2: 
- After step1, the IDE should open a browser after running the code successfully. 

<img src="step2.png">



Step 3: 
- Click on the interface and create satellites and devices: devices are created when hovering on the circumference of the planet and satellites are created when hovering above the planet. 
- When planets are in range of devices, a red line should be marked as shown. 
<img src="step3.png">



Step 4: 
- To create a file, click on the device and click 'create file'. 
- Once the file is created, information can be added and then sent to a satellite in range after opening the file. 
<img src="step4.png">


Step 5: 
- After the file is sent to a satellite, a unit of time should be clicked next to the "simulate for..." icon to check the progress of the transfer after a unit of time. 
