package blackout;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import unsw.blackout.BlackoutController;
import unsw.blackout.FileTransferException;
import unsw.blackout.RelaySatellite;
import unsw.response.models.FileInfoResponse;
import unsw.response.models.EntityInfoResponse;
import unsw.utils.Angle;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static unsw.utils.MathsHelper.RADIUS_OF_JUPITER;
import java.util.Arrays;
import static blackout.TestHelpers.assertListAreEqualIgnoringOrder;

@TestInstance(value = Lifecycle.PER_CLASS)
public class Task2ExtraTests {
    @Test
    public void testRelayMovement() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "RelaySatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(180));

        // moves in positive direction
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(180), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(181.23), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(182.46), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(183.69), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        
        // edge case
        controller.simulate(5);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(189.82), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(191.05), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        
        // goes back down
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(189.82), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(5);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(183.69), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
    }

    //relay movement start out of range 
    @Test
    public void testRelayMovement2() {
        
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "RelaySatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(135));

        // moves into the 140-190 degree range 
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(135), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(136.23), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(137.46), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(138.68), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(139.91), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        
        //in 140-190 degree range 
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(141.14), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(10);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(153.41), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(28);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(187.79), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        
        //edge case
        controller.simulate(2);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(190.24), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        
        // goes back down
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(189.01), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(38);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(142.37), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(141.14), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        
        // goes back up
        controller.simulate(1);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(139.91), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
        controller.simulate(30);
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(176.74), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite1"));
    }

    
    @Test
    public void testSendFileUsingRelayInBetween() {
       
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "StandardSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createSatellite("Satellite2", "StandardSatellite", 20000 + RADIUS_OF_JUPITER, Angle.fromDegrees(300));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createSatellite("Satellite4", "ShrinkingSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(240));

        //withuot relay satellite - check entities in range 
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "Satellite2"), controller.communicableEntitiesInRange("Satellite1"));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "Satellite1"), controller.communicableEntitiesInRange("Satellite2"));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite2", "Satellite1"), controller.communicableEntitiesInRange("DeviceB"));

        //creation of relay 
        controller.createSatellite("Satellite3", "RelaySatellite", 19000 + RADIUS_OF_JUPITER, Angle.fromDegrees(280));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite4", "DeviceB", "Satellite2", "Satellite1"), controller.communicableEntitiesInRange("Satellite3"));
        
        //send from DeviceB via relay to Satellite 4 
        String msg = "hello";
        controller.addFileToDevice("DeviceB", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceB", "Satellite4"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite4").getFiles().get("FileAlpha"));
    }




    @Test
    public void testQuantumBehaviour() {
        // just some of them... you'll have to test the rest
        BlackoutController controller = new BlackoutController();

        // Creates 1 satellite and 2 devices
        controller.createSatellite("Satellite1", "ShrinkingSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceA", "HandheldDevice", Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(315));

        // uploads at a rate of 15 per minute so we'll give it 21 bytes which when compressed is 14 "hello quantum how are"
        String msg = "hello quantum how are";
        controller.addFileToDevice("DeviceA", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceA", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        // we still should have 6 bytes to send
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "hello quantum h", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        // now that we are done we should see shrinkage
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", msg, 14, true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        // sending file back down to other device it needs to send full 21 bytes, bandwidth out is 10 so it should take 3 ticks
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "Satellite1", "DeviceB"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        // we still should have 11 bytes to send
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "hello quan", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        // and still 1 more byte to send
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "hello quantum how ar", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        // done! and file size should not be shrunk, we aren't on the shrinking satellite
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "hello quantum how are", msg.length(), true), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));
    }


    @Test
    public void testSendShrinkingSatellieBandwidth() {
        
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "ShrinkingSatellite", 10000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));

        String msg = "Heyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy";
        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        String msg2 = "Heyyyyyyyyyyyyyyyyyyyyyyy2";
        controller.addFileToDevice("DeviceB", "FileAlpha2", msg2);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha2", "DeviceB", "Satellite1"));

        //must transfer lower data with 2 devices sending to 
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha2", "Heyyyyy", msg2.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha2"));
    }

    @Test
    public void testSendShrinkingException() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "ShrinkingSatellite", 10000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));

        String msg = "Heyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy";
        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyyyyyyyyyyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        
        //testing storage exception
        controller.simulate(1);
        String msg2 = "Heyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy2";
        controller.addFileToDevice("DeviceB", "FileAlpha2", msg2);
        assertThrows(FileTransferException.VirtualFileNoStorageSpaceException.class, () -> controller.sendFile("FileAlpha2", "DeviceB", "Satellite1"));
    }

}
