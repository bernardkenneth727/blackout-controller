package blackout;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import unsw.blackout.BlackoutController;
import unsw.blackout.FileTransferException;
import unsw.response.models.FileInfoResponse;
import unsw.response.models.EntityInfoResponse;
import unsw.utils.Angle;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static unsw.utils.MathsHelper.RADIUS_OF_JUPITER;
import java.util.Arrays;
import static blackout.TestHelpers.assertListAreEqualIgnoringOrder;

@TestInstance(value = Lifecycle.PER_CLASS)
public class Task2ExampleTests {
    @Test
    public void testEntitiesInRange() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "StandardSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createSatellite("Satellite2", "StandardSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(315));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));
        controller.createDevice("DeviceD", "HandheldDevice", Angle.fromDegrees(180));
        controller.createDevice("DeviceA", "DesktopDevice", Angle.fromDegrees(311)); //desktop device 
        controller.createSatellite("Satellite3", "StandardSatellite", 2000 + RADIUS_OF_JUPITER, Angle.fromDegrees(175));

        controller.createSatellite("Satellite4", "ShrinkingSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(0));
        controller.createSatellite("Satellite5", "StandardSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(0));
        controller.createDevice("DeviceE", "HandheldDevice", Angle.fromDegrees(1));
        controller.createDevice("DeviceF", "DesktopDevice", Angle.fromDegrees(3));



        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite2"), controller.communicableEntitiesInRange("Satellite1"));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite1"), controller.communicableEntitiesInRange("Satellite2"));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite2", "Satellite1"), controller.communicableEntitiesInRange("DeviceB"));

        //testing satellite compatability with devices 
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceD"), controller.communicableEntitiesInRange("Satellite3"));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceE", "DeviceF", "Satellite5"), controller.communicableEntitiesInRange("Satellite4"));
        //standard sat incompatible with desktop device
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceE", "Satellite4"), controller.communicableEntitiesInRange("Satellite5"));
    }

    @Test
    public void testEntitiesInRange2() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        //satelites same angle but diferrent height affecting difference in distance
        controller.createSatellite("Satellite1", "StandardSatellite", 150000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createSatellite("Satellite2", "StandardSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(311));

        //shows how a difference in height effects what is in range
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite1"), controller.communicableEntitiesInRange("Satellite2"));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite2"), controller.communicableEntitiesInRange("Satellite1"));
    }

    @Test
    public void testEntitiesInRangeWithRelay() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        
        controller.createSatellite("Satellite1", "StandardSatellite", 1000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createSatellite("Satellite2", "StandardSatellite", 20000 + RADIUS_OF_JUPITER, Angle.fromDegrees(300));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));
        controller.createDevice("DeviceD", "HandheldDevice", Angle.fromDegrees(240));
        
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite2"), controller.communicableEntitiesInRange("Satellite1"));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite1"), controller.communicableEntitiesInRange("Satellite2"));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite2", "Satellite1"), controller.communicableEntitiesInRange("DeviceB"));


        controller.createSatellite("Satellite3", "RelaySatellite", 19000 + RADIUS_OF_JUPITER, Angle.fromDegrees(280));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceD", "DeviceB", "DeviceC", "Satellite2", "Satellite1"), controller.communicableEntitiesInRange("Satellite3"));

        //after creation of relay satellite
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite2", "Satellite3", "DeviceD"), controller.communicableEntitiesInRange("Satellite1"));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite1", "Satellite3", "DeviceD"), controller.communicableEntitiesInRange("Satellite2"));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite2", "Satellite1", "Satellite3"), controller.communicableEntitiesInRange("DeviceB"));
    }

    @Test
    public void testSomeExceptionsForSend() {
        // just some of them... you'll have to test the rest
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "StandardSatellite", 5000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));
        controller.createDevice("DeviceA", "HandheldDevice", Angle.fromDegrees(311));
        controller.createDevice("DeviceD", "HandheldDevice", Angle.fromDegrees(315));
        controller.createDevice("DeviceF", "HandheldDevice", Angle.fromDegrees(190));

        String msg = "Hey";
        controller.addFileToDevice("DeviceC", "FileAlpha", msg);

        //file not found test
        assertThrows(FileTransferException.VirtualFileNotFoundException.class, () -> controller.sendFile("NonExistentFile", "DeviceC", "Satellite1"));

        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        controller.simulate(msg.length() * 2);

        //file already exists test 
        assertThrows(FileTransferException.VirtualFileAlreadyExistsException.class, () -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));

        String msg2 = "mesageeeeeee";
        controller.addFileToDevice("DeviceA", "FileAlpha2", msg2);
        String msg3 = "mesageeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee";
        controller.addFileToDevice("DeviceF", "FileAlpha3", msg3);
        String msg4 = "mmmmmmmmmmmmmmmmm";
        controller.addFileToDevice("DeviceD", "FileAlpha4", msg4);

        assertEquals(new FileInfoResponse("FileAlpha", "Hey", msg.length(), true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        //bandwidth test
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha2", "DeviceA", "Satellite1"));
        assertThrows(FileTransferException.VirtualFileNoBandwidthException.class, () -> controller.sendFile("FileAlpha4", "DeviceD", "Satellite1"));
        controller.simulate(1);

        assertEquals(new FileInfoResponse("FileAlpha2", "m", msg2.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha2"));

        controller.simulate(msg2.length());

        //out of range during transfer 
        assertEquals(null, controller.getInfo("Satellite1").getFiles().get("FileAlpha2"));

        //too much storage bytes test
        assertThrows(FileTransferException.VirtualFileNoStorageSpaceException.class, () -> controller.sendFile("FileAlpha3", "DeviceF", "Satellite1"));

    }

    @Test
    public void testMovement() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "StandardSatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(340));
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(340), 100 + RADIUS_OF_JUPITER, "StandardSatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(342.05), 100 + RADIUS_OF_JUPITER, "StandardSatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(344.10), 100 + RADIUS_OF_JUPITER, "StandardSatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(346.15), 100 + RADIUS_OF_JUPITER, "StandardSatellite"), controller.getInfo("Satellite1"));

        controller.createSatellite("Satellite2", "ShrinkingSatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(140));
        assertEquals(new EntityInfoResponse("Satellite2", Angle.fromDegrees(140), 100 + RADIUS_OF_JUPITER, "ShrinkingSatellite"), controller.getInfo("Satellite2"));
        controller.simulate();

        assertEquals(new EntityInfoResponse("Satellite2", Angle.fromDegrees(140.82), 100 + RADIUS_OF_JUPITER, "ShrinkingSatellite"), controller.getInfo("Satellite2"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite2", Angle.fromDegrees(141.64), 100 + RADIUS_OF_JUPITER, "ShrinkingSatellite"), controller.getInfo("Satellite2"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite2", Angle.fromDegrees(142.46), 100 + RADIUS_OF_JUPITER, "ShrinkingSatellite"), controller.getInfo("Satellite2"));
        
    }

    @Test
    public void testBasicSendExample() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        
        controller.createSatellite("Satellite1", "StandardSatellite", 10000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));

        String msg = "Hey";
        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(msg.length() * 2);
        assertEquals(new FileInfoResponse("FileAlpha", msg, msg.length(), true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "Satellite1", "DeviceB"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        controller.simulate(msg.length());
        assertEquals(new FileInfoResponse("FileAlpha", msg, msg.length(), true), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

    }

    @Test
    public void testSend2() {
        // Task 2
        // Example from the specification
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "StandardSatellite", 4000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "HandheldDevice", Angle.fromDegrees(320));

        String msg = "Hey";

        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate();

        assertEquals(new FileInfoResponse("FileAlpha", "H", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate();

        assertEquals(new FileInfoResponse("FileAlpha", "He", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(1);

        assertEquals(new FileInfoResponse("FileAlpha", "Hey", msg.length(), true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(1);

        assertEquals(new FileInfoResponse("FileAlpha", "Hey", msg.length(), true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "Satellite1", "DeviceB"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "H", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        //Out of range example - should delete file
        controller.simulate(2);
        assertEquals(null, controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

    }
}
