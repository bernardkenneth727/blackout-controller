package blackout;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import unsw.blackout.BlackoutController;
import unsw.blackout.FileTransferException;
import unsw.response.models.FileInfoResponse;
import unsw.response.models.EntityInfoResponse;
import unsw.utils.Angle;
import unsw.utils.ZippedFileUtils;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static unsw.utils.MathsHelper.RADIUS_OF_JUPITER;
import java.util.Arrays;
import static blackout.TestHelpers.assertListAreEqualIgnoringOrder;

@TestInstance(value = Lifecycle.PER_CLASS)
public class Task3Tests {
    @Test
    public void creatingTest() {
       
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "StandardSatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(340));
        controller.createSatellite("Satellite2", "RelaySatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(300));
        controller.createDevice("DeviceA", "HandheldDevice", Angle.fromDegrees(30));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(180));
        controller.createDevice("DeviceC", "DesktopDevice", Angle.fromDegrees(330));

        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite1", "Satellite2"), controller.listSatelliteIds());
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceA", "DeviceB", "DeviceC"), controller.listDeviceIds());

        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(340), 100 + RADIUS_OF_JUPITER, "StandardSatellite"), controller.getInfo("Satellite1"));
        assertEquals(new EntityInfoResponse("Satellite2", Angle.fromDegrees(300), 100 + RADIUS_OF_JUPITER, "RelaySatellite"), controller.getInfo("Satellite2"));

        controller.createSatellite("Satellite3", "ElephantSatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(350));
        assertListAreEqualIgnoringOrder(Arrays.asList("Satellite1", "Satellite2", "Satellite3"), controller.listSatelliteIds());
        assertEquals(new EntityInfoResponse("Satellite3", Angle.fromDegrees(350), 100 + RADIUS_OF_JUPITER, "ElephantSatellite"), controller.getInfo("Satellite3"));

        controller.createDevice("DeviceD", "CloudStorageDevice", Angle.fromDegrees(330));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceA", "DeviceB", "DeviceC", "DeviceD"), controller.listDeviceIds());
        assertEquals(new EntityInfoResponse("DeviceD", Angle.fromDegrees(330), RADIUS_OF_JUPITER, "CloudStorageDevice"), controller.getInfo("DeviceD"));
    }

    @Test
    public void testEntitiesInRange() {
        
        BlackoutController controller = new BlackoutController();

        //satelites same angle but diferrent height affecting difference in distance
        controller.createSatellite("Satellite1", "ElephantSatellite", 150000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createSatellite("Satellite2", "ShrinkingSatellite", 150000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "CloudStorageDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "DesktopDevice", Angle.fromDegrees(311));
        controller.createDevice("DeviceD", "HandheldDevice", Angle.fromDegrees(311));


        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite1", "DeviceD"), controller.communicableEntitiesInRange("Satellite2"));

        ////testing satellite compatability with devices  - doesn't support handheld device
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceB", "DeviceC", "Satellite2"), controller.communicableEntitiesInRange("Satellite1"));
        
    }


    @Test
    public void testMovementElephant() {
        BlackoutController controller = new BlackoutController();
        
        controller.createSatellite("Satellite1", "ElephantSatellite", 100 + RADIUS_OF_JUPITER, Angle.fromDegrees(340));
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(340), 100 + RADIUS_OF_JUPITER, "ElephantSatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(342.05), 100 + RADIUS_OF_JUPITER, "ElephantSatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(344.10), 100 + RADIUS_OF_JUPITER, "ElephantSatellite"), controller.getInfo("Satellite1"));
        controller.simulate();
        assertEquals(new EntityInfoResponse("Satellite1", Angle.fromDegrees(346.15), 100 + RADIUS_OF_JUPITER, "ElephantSatellite"), controller.getInfo("Satellite1"));
        
    }

    @Test
    public void testSendElephantBasic() {
        
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "ElephantSatellite", 10000 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "DesktopDevice", Angle.fromDegrees(320));

        String msg = "Hey";
        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate(msg.length() * 2);
        assertEquals(new FileInfoResponse("FileAlpha", msg, msg.length(), true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "Satellite1", "DeviceB"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

        controller.simulate(msg.length());
        assertEquals(new FileInfoResponse("FileAlpha", msg, msg.length(), true), controller.getInfo("DeviceB").getFiles().get("FileAlpha"));

    }

    @Test
    public void testSendElephant2() {
        
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "ElephantSatellite", 500 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "DesktopDevice", Angle.fromDegrees(315));

        String msg = "Heyyyyyyyyyyyyyyyyyyyyyyyy1";

        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        controller.simulate();
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));

        //goes out of range - file should stay
        controller.simulate();
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        controller.simulate(168);
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        
        //in range again - should complete 
        controller.simulate(1);
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyyyyyyyyyyyyyyyyyy1", msg.length(), true), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
    }

    @Test
    public void testSendElephant3() {
        
        BlackoutController controller = new BlackoutController();

        controller.createSatellite("Satellite1", "ElephantSatellite", 500 + RADIUS_OF_JUPITER, Angle.fromDegrees(320));
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(310));
        controller.createDevice("DeviceC", "DesktopDevice", Angle.fromDegrees(315));

        
        String msg = "Heyyyyyyyyyyyyyyyyyyyyyyyyy1"; //28 bytes
        controller.addFileToDevice("DeviceC", "FileAlpha", msg);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha", "", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        
        String msg2 = "Heyyyyyyyyyyyyyyyyyyyyyyyyy2"; //28 bytes
        controller.addFileToDevice("DeviceC", "FileAlpha2", msg2);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha2", "DeviceC", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha2", "", msg2.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha2"));

        controller.simulate();
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        assertEquals(new FileInfoResponse("FileAlpha2", "Heyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha2"));
        
        //goes out of range - files should stay
        controller.simulate();
        assertEquals(new FileInfoResponse("FileAlpha", "Heyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha"));
        assertEquals(new FileInfoResponse("FileAlpha2", "Heyyyyyyyy", msg.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha2"));
        

        //creating new file and sending to elephant sat - should delete transient file to make space 
        controller.createDevice("DeviceD", "DesktopDevice", Angle.fromDegrees(325));
        String msg3 = "Heyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy3"; //38 bytes
        controller.addFileToDevice("DeviceD", "FileAlpha3", msg3);
        assertDoesNotThrow(() -> controller.sendFile("FileAlpha3", "DeviceD", "Satellite1"));
        assertEquals(new FileInfoResponse("FileAlpha3", "", msg3.length(), false), controller.getInfo("Satellite1").getFiles().get("FileAlpha3"));
        
    }


    @Test
    public void basicFileSupportCloudStorageDevice() {
        BlackoutController controller = new BlackoutController();

        controller.createDevice("DeviceA", "CloudStorageDevice", Angle.fromDegrees(330));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceA"), controller.listDeviceIds());
        assertEquals(new EntityInfoResponse("DeviceA", Angle.fromDegrees(330), RADIUS_OF_JUPITER, "CloudStorageDevice"), controller.getInfo("DeviceA"));

        String msg = "Hello Worlddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd1";
        controller.addFileToDevice("DeviceA", "My first file!", msg);
        String zipMsg = ZippedFileUtils.zipFile(msg); 
        assertEquals(new FileInfoResponse("My first file!", zipMsg, zipMsg.length(), true), controller.getInfo("DeviceA").getFiles().get("My first file!"));
    

        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(330));
        String msg2 = "Hello Worlddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd2";
        controller.addFileToDevice("DeviceB", "My first file!", msg2);
        
        assertEquals(new FileInfoResponse("My first file!", msg2, msg.length(), true), controller.getInfo("DeviceB").getFiles().get("My first file!"));
    }



    @Test
    public void FileSendZipCloudStorageDevice() {
        // Task 1
        BlackoutController controller = new BlackoutController();

        controller.createDevice("DeviceA", "CloudStorageDevice", Angle.fromDegrees(330));
        assertListAreEqualIgnoringOrder(Arrays.asList("DeviceA"), controller.listDeviceIds());
        assertEquals(new EntityInfoResponse("DeviceA", Angle.fromDegrees(330), RADIUS_OF_JUPITER, "CloudStorageDevice"), controller.getInfo("DeviceA"));

        //check file zips when created
        String msg = "Hello Worlddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd1";
        controller.addFileToDevice("DeviceA", "My first file!", msg);
        String zipMsg = ZippedFileUtils.zipFile(msg); 
        assertEquals(new FileInfoResponse("My first file!", zipMsg, zipMsg.length(), true), controller.getInfo("DeviceA").getFiles().get("My first file!"));
    
        //send zip file to Satellite
        controller.createSatellite("Satellite1", "ShrinkingSatellite", 5000 + RADIUS_OF_JUPITER, Angle.fromDegrees(330));
        assertDoesNotThrow(() -> controller.sendFile("My first file!", "DeviceA", "Satellite1"));
        assertEquals(new FileInfoResponse("My first file!", "", zipMsg.length(), false), controller.getInfo("Satellite1").getFiles().get("My first file!"));
        controller.simulate();
        assertEquals(new FileInfoResponse("My first file!", "H4sIAAAAAAAAAPN", zipMsg.length(), false), controller.getInfo("Satellite1").getFiles().get("My first file!"));
        controller.simulate(5);
        assertEquals(new FileInfoResponse("My first file!", zipMsg, zipMsg.length(), true), controller.getInfo("Satellite1").getFiles().get("My first file!"));

        //send zip file from Satellite to device (non-cloud)- should unzip 
        controller.createDevice("DeviceC", "LaptopDevice", Angle.fromDegrees(345));
        assertDoesNotThrow(() -> controller.sendFile("My first file!", "Satellite1", "DeviceC"));
        assertEquals(new FileInfoResponse("My first file!", "", zipMsg.length(), false), controller.getInfo("DeviceC").getFiles().get("My first file!"));
        controller.simulate(5);
        assertEquals(new FileInfoResponse("My first file!", msg, msg.length(), true), controller.getInfo("DeviceC").getFiles().get("My first file!"));

        //send unzip file to cloudstoragedevice - should zip only when done
        controller.createDevice("DeviceB", "LaptopDevice", Angle.fromDegrees(360));
        String msg2 = "Hello Worlddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd2";
        controller.addFileToDevice("DeviceB", "My first file!2", msg2);
        assertDoesNotThrow(() -> controller.sendFile("My first file!2", "DeviceB", "Satellite1"));
        assertEquals(new FileInfoResponse("My first file!2", "", msg2.length(), false), controller.getInfo("Satellite1").getFiles().get("My first file!2"));
        controller.simulate(6);
        assertEquals(new FileInfoResponse("My first file!2", msg2, msg2.length(), true), controller.getInfo("Satellite1").getFiles().get("My first file!2"));
        assertDoesNotThrow(() -> controller.sendFile("My first file!2", "Satellite1", "DeviceA"));
        controller.simulate(1);
        assertEquals(new FileInfoResponse("My first file!2", "Hello Worl", msg2.length(), false), controller.getInfo("DeviceA").getFiles().get("My first file!2"));
        controller.simulate(7);
        assertEquals(new FileInfoResponse("My first file!2", ZippedFileUtils.zipFile(msg2), ZippedFileUtils.zipFile(msg2).length(), true), controller.getInfo("DeviceA").getFiles().get("My first file!2"));

    }
}

