package unsw.blackout;

import unsw.utils.Angle;
import unsw.utils.MathsHelper;
import unsw.response.models.FileInfoResponse;

public class ShrinkingSatellite extends StorageSatellite {
    
    private final int maxRange = 200000;
    private final double linearVelocity = 1000; 
    private final int maxStorage = 150; 
    private final int maxBandwidthReceive = 15; 
    private final int maxBandwidthSend = 10; 

    public ShrinkingSatellite(Angle position, double height, String id) {
        super(position, height, id); 
        setType("ShrinkingSatellite");
    }

    public void angularMovement(int mins) {
        double radiansMoved = (linearVelocity/getHeight()) * mins; 
        Angle radiansShifted = Angle.fromRadians(radiansMoved); 
        Angle originalAngle = getPosition(); 
        Angle newPos = originalAngle.add(radiansShifted);
        
        if (1 == newPos.compareTo(Angle.fromDegrees(360.0))) {
            newPos = newPos.subtract(Angle.fromDegrees(360.0)); 
        } 
        
        setPosition(newPos);
    }

    public int getMaxRange() {
        return maxRange; 
    }

    
    public int maxBytesSendPerminute() {
        return maxBandwidthSend; 
    }

    public int maxBytesReceivePerminute() {
        return maxBandwidthReceive; 
    }
    
    public int currentBytesSendPerminute() {
        if (numObjectsSendingTo() == 0) {
            return maxBandwidthSend;
        } else {
            return (maxBandwidthSend/numObjectsSendingTo()); 
        }
    }
    
    public int currentBytesReceivePerminute() {
        if (numObjectsReceivingFrom() == 0) {
            return maxBandwidthReceive; 
        } else {
            return (maxBandwidthReceive/numObjectsReceivingFrom()); 
        }
    }

    public int getMaxStorage() {
        return this.maxStorage; 
    }
    
    public void addChar(String filename, char increment) { 
        FileInfoResponse file = getFile(filename);
        String data = file.getData() + increment; 
        double fileSize = file.getFileSize();
        boolean hasTransferCompleted; 
        if (data.length() == fileSize) {
            hasTransferCompleted = true; 
            if (data.toLowerCase().contains("quantum")) {
                fileSize = (fileSize / 3) * 2; 
            }
        } else {
            hasTransferCompleted = false; 
        }

        removeFile(filename);
        addFile(filename, data, (int)Math.round(fileSize), hasTransferCompleted);
    }


    public boolean hasEnoughStorage(String originalFileData) {
        if (getStorage() + originalFileData.length() > getMaxStorage()) {
            return false;
        } else {
            return true; 
        }
    }
    
    public void inRangeUpdates(FileInfoResponse file) {
        
    }

    public void outOfRangeUpdates(String filename, FileInfoResponse file, Device fromDevice) {
        deleteFromFileTransferMap(filename);
        removeFile(filename);
        stopReceivingFromObject();;
        deleteStorage(file.getFileSize());        
    }

    public void outOfRangeUpdates(String filename, FileInfoResponse file, StorageSatellite fromSatellite) {
        deleteFromFileTransferMap(filename);
        removeFile(filename);
        fromSatellite.stopSendingToObject();
        deleteStorage(file.getFileSize());
    }

    public boolean isReachable(Satellite destSatellite) { 
        if (MathsHelper.isVisible(destSatellite.getHeight(), destSatellite.getPosition(), getHeight(), getPosition())) {
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), destSatellite.getHeight(), destSatellite.getPosition()); 
            if (dist <= getMaxRange()) {
                return true; 
            }
        }

        return false; 
    }

    public boolean isReachable(Device destDevice) {
        if (MathsHelper.isVisible(getHeight(), getPosition(), destDevice.getPosition())) {
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), destDevice.getPosition()); 
            if (dist <= getMaxRange()) {
                return true; 
            }
        }
        
        return false; 
    }
}