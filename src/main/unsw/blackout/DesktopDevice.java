package unsw.blackout;

import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;
import unsw.utils.ZippedFileUtils;

public class DesktopDevice extends Device {
    private final int maxRange = 200000; 

    public DesktopDevice(Angle position, String id) {
        super(position, id); 
        setType("DesktopDevice");
    }

    public int getMaxRange() {
        return maxRange; 
    }



    
}
