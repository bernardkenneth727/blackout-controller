package unsw.blackout;

import unsw.utils.Angle;
import unsw.utils.MathsHelper;

public class RelaySatellite extends Satellite {
   
    private final int maxRange = 200000;
    private final double linearVelocity = 1500; 
    private int surpassed190Threshold = 0; 
    private int surpassed140Threshold = 0; 
    private boolean goClockwise = false;  

    public RelaySatellite(Angle position, double height, String id) {
        super(position, height, id); 
        setType("RelaySatellite");
    }

    public void angularMovement(int mins) {
        if (!inRegionNow()) {
            if (-1 == getPosition().compareTo(Angle.fromDegrees(345.0)) && 1 == getPosition().compareTo(Angle.fromDegrees(190.0))) {
                angularMovementClockwise(mins);
            } else if (1 == getPosition().compareTo(Angle.fromDegrees(345.0)) || -1 == getPosition().compareTo(Angle.fromDegrees(140.0))) {
                angularMovementAntiClockwise(mins);
            }
        } else {
            if (goClockwise) {
                angularMovementClockwise(mins); 
                if (surpassed140Threshold == 1) {
                    goClockwise = false; 
                } 
            } else if (!goClockwise) {
                angularMovementAntiClockwise(mins); 
                if (surpassed190Threshold == 1) {
                    goClockwise = true; 
                }
            }
        }
    }

    public boolean inRegionNow() {
        Angle angleNow = getPosition(); 

        if (angleNow.toDegrees() >= 140.0 && angleNow.toDegrees() <= 190.0) {
            return true;  
        } else { 
            return false; 
        }
    }

    public void angularMovementAntiClockwise(int mins) {
        double radiansMoved = (linearVelocity/getHeight()) * (double) mins; 
        Angle radiansShifted = Angle.fromRadians(radiansMoved); 
        Angle originalAngle = getPosition(); 
        Angle newPos = originalAngle.add(radiansShifted);
        
        if (1 == newPos.compareTo(Angle.fromDegrees(360.0))) {
            newPos = newPos.subtract(Angle.fromDegrees(360.0)); 
        } 
        
        setPosition(newPos);

        if (1 == newPos.compareTo(Angle.fromDegrees(190.0)) || 0 == newPos.compareTo(Angle.fromDegrees(190.0))) {
            surpassed190Threshold = 1; 
            surpassed140Threshold = 0; 
        }
    }

    public void angularMovementClockwise(int mins) {
        double radiansMoved = (linearVelocity/getHeight()) * mins;  
        Angle radiansShifted = Angle.fromRadians(radiansMoved); 
        Angle originalAngle = getPosition(); 
        Angle newPos = originalAngle.subtract(radiansShifted);
        
        if (1 == newPos.compareTo(Angle.fromDegrees(360.0))) {
            newPos = newPos.subtract(Angle.fromDegrees(360.0)); 
        } 
        
        setPosition(newPos);

        if (-1 == newPos.compareTo(Angle.fromDegrees(140.0)) || 0 == newPos.compareTo(Angle.fromDegrees(140.0))) {
            surpassed190Threshold = 0; 
            surpassed140Threshold = 1;
        } 
    }

    public int getMaxRange() {
        return maxRange; 
    }

    public boolean isReachable(Satellite destSatellite) { 
        if (MathsHelper.isVisible(destSatellite.getHeight(), destSatellite.getPosition(), getHeight(), getPosition())) {
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), destSatellite.getHeight(), destSatellite.getPosition()); 
            if (dist <= getMaxRange()) {
                return true; 
            }
        }

        return false; 
    }

    public boolean isReachable(Device destDevice) {
        if (MathsHelper.isVisible(getHeight(), getPosition(), destDevice.getPosition())) {
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), destDevice.getPosition()); 
            if (dist <= getMaxRange()) {
                return true; 
            }
        }
        
        return false; 
    }

    
}
