package unsw.blackout;

import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;
import unsw.utils.ZippedFileUtils;

public class HandheldDevice extends Device {
    private final int maxRange = 50000;
    
    public HandheldDevice(Angle position, String id) {
        super(position, id); 
        setType("HandheldDevice");
    }

    public int getMaxRange() {
        return maxRange; 
    }


    
}
