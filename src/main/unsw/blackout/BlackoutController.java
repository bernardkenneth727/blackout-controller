package unsw.blackout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import unsw.blackout.FileTransferException.VirtualFileAlreadyExistsException;
import unsw.blackout.FileTransferException.VirtualFileNoBandwidthException;
import unsw.blackout.FileTransferException.VirtualFileNoStorageSpaceException;
import unsw.blackout.FileTransferException.VirtualFileNotFoundException;
import unsw.response.models.EntityInfoResponse;
import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;

public class BlackoutController {
    private Map<String, Device> deviceMap = new HashMap<>();
    private Map<String, Satellite> satelliteMap = new HashMap<>();
    
    /** Helper function to check if two IDs are within range
     * 
     * @param fromId
     * @param toId
     * @return boolean
     */
    private boolean isInRange(String fromId, String toId) { 
        boolean reachable = false; 
        List<String> rangeList; 

        rangeList = communicableEntitiesInRange(fromId); 
        for (String id : rangeList) {
            if (id.equals(toId)) {
                reachable = true; 
            }
        }
        return reachable; 
    }

    /** Helper function to add to device map 
     * 
     * @param id
     * @param device
     */
    private void addToDeviceMap(String id, Device device) {
        deviceMap.put(id, device); 
    }

    /** Helper function to delete from device map 
     * 
     * @param id
     */
    private void deleteFromDeviceMap(String id) {
        deviceMap.remove(id); 
    }

    /**
     * Helper function to add to satellite map 
     * @param id
     * @param satellite
     */
    private void addToSatelliteMap(String id, Satellite satellite) {
        satelliteMap.put(id, satellite); 
    }

    /**
     * Helper function to delete from satellite map 
     * @param id
     */
    private void deleteFromSatelliteMap(String id) {
        satelliteMap.remove(id); 
    }

    /**
     * Helper function to return device based on deviceName 
     * @param deviceName
     * @return device 
     */
    private Device getDevice(String deviceName) {
        Device device = null; 
        for (Map.Entry<String, Device> entry : deviceMap.entrySet()) {
            if (deviceName.equals(entry.getKey())) {
                device = entry.getValue(); 
            }
        }
        return device; 
    }

    /**
     * Helper function to return Satellite based on name 
     * @return satellite 
     */
    private Satellite getSatellite(String satelliteName) {
        Satellite satellite = null; 
        for (Map.Entry<String, Satellite> entry : satelliteMap.entrySet()) {
            if (satelliteName.equals(entry.getKey())) {
                satellite = entry.getValue(); 
            }
        }
        return satellite; 
    }
    
    public void createDevice(String deviceId, String type, Angle position) {
        // TODO: Task 1a)
        
        Device newDevice = null; 

        if (type.equals("DesktopDevice")) {
            newDevice = new DesktopDevice(position, deviceId); 
        } else if (type.equals("HandheldDevice")) {
            newDevice = new HandheldDevice(position, deviceId); 
        } else if (type.equals("LaptopDevice")) {
            newDevice = new LaptopDevice(position, deviceId); 
        } else if (type.equals("CloudStorageDevice")) {
            newDevice = new CloudStorageDevice(position, deviceId); 
        }
        addToDeviceMap(deviceId, newDevice);
    }

    public void removeDevice(String deviceId) {
        // TODO: Task 1b)
        deleteFromDeviceMap(deviceId);
    }

    public void createSatellite(String satelliteId, String type, double height, Angle position) {
        // TODO: Task 1c) 

        Satellite newSatellite = null; 

        if (type.equals("RelaySatellite")) {
            newSatellite = new RelaySatellite(position, height, satelliteId); 
        } else if (type.equals("StandardSatellite")) {
            newSatellite = new StandardSatellite(position, height, satelliteId); 
        } else if (type.equals("ShrinkingSatellite")) {
            newSatellite = new ShrinkingSatellite(position, height, satelliteId); 
        } else if (type.equals("ElephantSatellite")) {
            newSatellite = new ElephantSatellite(position, height, satelliteId); 
        }

        addToSatelliteMap(satelliteId, newSatellite);
    }

    public void removeSatellite(String satelliteId) {
        // TODO: Task 1d)
        deleteFromSatelliteMap(satelliteId);
    }

    public List<String> listDeviceIds() {
        // TODO: Task 1e)
        List<String> List = new ArrayList<String>(); 
        String name = null; 

        for (Map.Entry<String, Device> entry : deviceMap.entrySet()) {
            name = entry.getKey(); 
            List.add(name);
        }

        return List; 
    }

    public List<String> listSatelliteIds() {
        // TODO: Task 1f)
        List<String> List = new ArrayList<String>(); 
        String name = null; 

        for (Map.Entry<String, Satellite> entry : satelliteMap.entrySet()) {
            name = entry.getKey(); 
            List.add(name);
        }
        
        return List; 
    }

    public void addFileToDevice(String deviceId, String filename, String content) {
        // TODO: Task 1g)

        Device device = getDevice(deviceId);
        device.createFile(filename, content);
    }

    public EntityInfoResponse getInfo(String id) {
        // TODO: Task 1h)
        Device device = null;
        Satellite satellite = null;
        EntityInfoResponse info = null; 

        device = getDevice(id); 

        if (device != null) {
            info = new EntityInfoResponse(id, device.getPosition(), device.getHeight(), device.getType(), device.getFiles()); 
        } else {
            satellite = getSatellite(id);
            info = new EntityInfoResponse(id, satellite.getPosition(), satellite.getHeight(), satellite.getType(), satellite.getFiles()); 
        }
        return info; 
    }

    public void simulate() {
        // TODO: Task 2a)

        List<String> SatelliteList = new ArrayList<String>(); 
        SatelliteList = listSatelliteIds(); 
        for (String item : SatelliteList) {
            getSatellite(item).angularMovement(1);
        }

        String toId = null; 
        for (String item : SatelliteList) { 
            toId = item; 

            if (!getSatellite(item).getType().equals("RelaySatellite")) { 
                Map<String, String> fileTransferInProgressMap = ((StorageSatellite) getSatellite(item)).getFilesInProgress(); 
                String fileName = null; 
                String fromId = null; 
                for (Map.Entry<String, String> entry : fileTransferInProgressMap.entrySet()) {
                    fileName = entry.getKey(); 
                    fromId = entry.getValue(); 
                    try {
                        sendFile(fileName, fromId, toId);
                    } catch (FileTransferException e) {
                        e.printStackTrace(); 
                    }
                }
            }
        }

        List<String> DeviceList = new ArrayList<String>(); 
        DeviceList = listDeviceIds();  
        for (String item : DeviceList) {
            toId = item; 

            Map<String, String> fileTransferInProgressMap = getDevice(item).getFilesInProgress(); 
            String fileName = null; 
            String fromId = null; 
            for (Map.Entry<String, String> entry : fileTransferInProgressMap.entrySet()) {
                fileName = entry.getKey(); 
                fromId = entry.getValue(); 
                try {
                    sendFile(fileName, fromId, toId);
                } catch (FileTransferException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Simulate for the specified number of minutes.
     * You shouldn't need to modify this function.
     */
    public void simulate(int numberOfMinutes) {
        for (int i = 0; i < numberOfMinutes; i++) {
            simulate();
        }
    }

    public List<String> communicableEntitiesInRange(String id) {
        // TODO Task 2b)

        List<String> rangeList = new ArrayList<String>(); 
        Device srcDevice = null;

        srcDevice = getDevice(id);
        if (srcDevice != null) {
            for (Map.Entry<String, Satellite> entry : satelliteMap.entrySet()) {
                if (!rangeList.contains(entry.getKey())) { 
                    Satellite destSatellite = entry.getValue(); 
                    if (srcDevice.isReachable(destSatellite)) {
                        rangeList.add(destSatellite.getId());
                        if (destSatellite.getType().equals("RelaySatellite")) {
                            Satellite srcRelay = destSatellite; 
                            for (Map.Entry<String, Satellite> entry2 : satelliteMap.entrySet()) {
                                destSatellite = entry2.getValue(); 
                                if (!rangeList.contains(destSatellite.getId())) {
                                    if (srcRelay.isReachable(destSatellite)) {
                                        rangeList.add(destSatellite.getId()); 
                                    }
                                }
                            }
                        }
                    }    
                }                     
            }                
        } else { 
            Satellite srcSatellite = null; 
            srcSatellite = getSatellite(id);

            if (srcSatellite != null ) {
                for (Map.Entry<String, Satellite> entry : satelliteMap.entrySet()) {
                    if (!(id.equals(entry.getKey())) && !rangeList.contains(entry.getKey())) {
                        Satellite destSatellite = entry.getValue(); 
                        if (srcSatellite.isReachable(destSatellite)) { 
                            rangeList.add(destSatellite.getId()); 
                            if (destSatellite.getType().equals("RelaySatellite")) {
                                Satellite srcRelay = destSatellite; 
                                for (Map.Entry<String, Satellite> entry2 : satelliteMap.entrySet()) {
                                    destSatellite = entry2.getValue(); 
                                    if (!rangeList.contains(destSatellite.getId()) && !(destSatellite.equals(srcSatellite))) {
                                        if (srcRelay.isReachable(destSatellite)) {
                                            rangeList.add(destSatellite.getId()); 
                                        }
                                    }
                                }

                                for (Map.Entry<String, Device> entry2 : deviceMap.entrySet()) {
                                    Device destDevice = entry2.getValue();
                                    if (!rangeList.contains(destDevice.getId())) {
                                        if (srcRelay.isReachable(destDevice)) {
                                            rangeList.add(destDevice.getId()); 
                                        }
                                    }
                                }
                            }        
                        }     
                    }
                }
                
                for (Map.Entry<String, Device> entry : deviceMap.entrySet()) {
                    Device destDevice = entry.getValue(); 
                    if (!rangeList.contains(destDevice.getId())) {
                        if (srcSatellite.isReachable(destDevice)) {
                            rangeList.add(destDevice.getId());
                        }
                    }
                }
            }
        }
        return rangeList;
    }

    public void sendFile(String fileName, String fromId, String toId) throws FileTransferException {
        
        Device srcDevice = null;
        srcDevice = getDevice(fromId);
        FileInfoResponse file = null; 
        String originalFileData = null; 
 
        if (srcDevice != null) { 
            file = srcDevice.getFile(fileName); 
            if (file == null) {
                throw new VirtualFileNotFoundException(fileName); 
            }
            if (!file.hasTransferCompleted()) {
                throw new VirtualFileNotFoundException(fileName); 
            }

            originalFileData = file.getData(); 

            StorageSatellite destSatellite = (StorageSatellite) getSatellite(toId); 
            file = destSatellite.getFile(fileName); 
            if (file == null) {
                if (!destSatellite.hasEnoughStorage(originalFileData)) { //
                    throw new VirtualFileNoStorageSpaceException(fileName); 
                }
                if (destSatellite.numObjectsReceivingFrom() < destSatellite.maxBytesReceivePerminute()) { 
                    destSatellite.newFiletoUpload(srcDevice.getId(), null, fileName, originalFileData);;
                } else {
                    throw new VirtualFileNoBandwidthException(fileName); 
                }
            } else {
                if (file.getData().equals(originalFileData)) {
                    throw new VirtualFileAlreadyExistsException(fileName); 
                } else if (!isInRange(fromId, toId)) {   
                    destSatellite.outOfRangeUpdates(fileName, file, srcDevice);
                } else {
                    destSatellite.inRangeUpdates(file);
                    destSatellite.updateFile(fileName, originalFileData);
                }
            }
        }

        StorageSatellite srcSatellite = null;
        srcSatellite = (StorageSatellite) getSatellite(fromId);

        if (srcSatellite != null) {
            file = srcSatellite.getFile(fileName); 
            if (file == null) {
                throw new VirtualFileNotFoundException(fileName); 
            }
            if (!file.hasTransferCompleted()) {
                throw new VirtualFileNotFoundException(fileName); 
            }

            originalFileData = file.getData(); 

            StorageSatellite destSatellite = (StorageSatellite) getSatellite(toId); 
            if (destSatellite != null) {
                file = destSatellite.getFile(fileName); 
                if (file == null) {
                    if (!destSatellite.hasEnoughStorage(originalFileData)) { 
                        throw new VirtualFileNoStorageSpaceException(fileName); 
                    }
                    if (srcSatellite.numObjectsSendingTo() < srcSatellite.maxBytesSendPerminute()) {
                        destSatellite.newFiletoUpload(srcSatellite.getId(), srcSatellite, fileName, originalFileData);
                    } else {
                        throw new VirtualFileNoBandwidthException(fileName); 
                    }
                } else {
                    if (file.getData().equals(originalFileData)) {
                        throw new VirtualFileAlreadyExistsException(fileName); 
                    } else if (!isInRange(fromId, toId)) {    
                        destSatellite.outOfRangeUpdates(fileName, file, srcSatellite);
                    } else {
                        destSatellite.inRangeUpdates(file);
                        destSatellite.updateFile(srcSatellite, fileName, originalFileData);
                    }
                }
            } else if (destSatellite == null) {
                Device destDevice = getDevice(toId); 
                file = destDevice.getFile(fileName); 

                if (file == null) {
                    if (destDevice.newFileToUpload(fileName, srcSatellite, originalFileData)) {
                    } else {
                        throw new VirtualFileNoBandwidthException(fileName); 
                    }
                } else {
                    if (file.getData().equals(originalFileData)) {
                        throw new VirtualFileAlreadyExistsException(fileName); 
                    } else if (!isInRange(fromId, toId)) {   
                        destDevice.outOfRangeUpdates(fileName, srcSatellite);
                    } else {
                        destDevice.updateFile(srcSatellite, fileName, originalFileData);
                    }
                }
            }
        }
    }  
}
    