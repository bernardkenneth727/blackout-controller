package unsw.blackout;

import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;
import unsw.utils.ZippedFileUtils;

public class CloudStorageDevice extends Device {
    private final int maxRange = 200000; 

    public CloudStorageDevice(Angle position, String id) {
        super(position, id); 
        setType("CloudStorageDevice");
    }

    public int getMaxRange() {
        return maxRange; 
    }

    @Override
    public void createFile(String filename, String content) {
        content = ZippedFileUtils.zipFile(content); 
        int size = content.length(); 
        FileInfoResponse file = new FileInfoResponse(filename, content, size, true);
        addToFileMap(filename, file);
    }

    @Override
    public boolean addChar(String filename, char increment) { 
        FileInfoResponse file = getFile(filename);
        String data = file.getData() + increment; 
        int fileSize = file.getFileSize();
        boolean hasTransferCompleted; 
        if (data.length() == fileSize) {
            hasTransferCompleted = true; 
            if (!ZippedFileUtils.isZipped(data)) {
                data = ZippedFileUtils.zipFile(data); 
            }
            removeFromFileMap(filename);
            FileInfoResponse newFile = new FileInfoResponse(filename, data, data.length(), hasTransferCompleted);
            addToFileMap(filename, newFile);
            return true; 
        } else {
            hasTransferCompleted = false; 
            removeFromFileMap(filename);
            FileInfoResponse newFile = new FileInfoResponse(filename, data, fileSize, hasTransferCompleted);
            addToFileMap(filename, newFile);
            return false; 
        }
    }

}
