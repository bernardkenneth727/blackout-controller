package unsw.blackout;

import unsw.utils.Angle;
import unsw.utils.MathsHelper;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import unsw.response.models.FileInfoResponse;

public class ElephantSatellite extends StorageSatellite {
    
    private final int maxRange = 400000;
    private final double linearVelocity = 2500; 
    private final int maxStorage = 90; 
    private final int maxBandwidthReceive = 20; 
    private final int maxBandwidthSend = 20; 
    private Map<FileInfoResponse, Integer> transientFileMap = new HashMap<>();

    public ElephantSatellite(Angle position, double height, String id) {
        super(position, height, id); 
        setType("ElephantSatellite");
    }

    public void angularMovement(int mins) {
        double radiansMoved = (linearVelocity/getHeight()) * mins; 
        Angle radiansShifted = Angle.fromRadians(radiansMoved); 
        Angle originalAngle = getPosition(); 
        Angle newPos = originalAngle.add(radiansShifted);
        
        if (1 == newPos.compareTo(Angle.fromDegrees(360.0))) {
            newPos = newPos.subtract(Angle.fromDegrees(360.0)); 
        } 
        
        setPosition(newPos);
    }

    public int getMaxRange() {
        return maxRange; 
    }
    
    public int maxBytesSendPerminute() {
        return maxBandwidthSend; 
    }

    public int maxBytesReceivePerminute() {
        return maxBandwidthReceive; 
    }
    
    public int currentBytesSendPerminute() {
        
        if (numObjectsSendingTo() == 0) {
            return maxBandwidthSend;
        } else {
            return (maxBandwidthSend/numObjectsSendingTo()); 
        }
    }
    
    public int currentBytesReceivePerminute() {
        if (numObjectsReceivingFrom() == 0) {
            return maxBandwidthReceive; 
        } else {
            return (maxBandwidthReceive/numObjectsReceivingFrom()); 
        }
    }

    public int getMaxStorage() {
        return this.maxStorage; 
    }
    
    public void addChar(String filename, char increment) {
        FileInfoResponse file = getFile(filename);
        String data = file.getData() + increment; 
        int fileSize = file.getFileSize();
        boolean hasTransferCompleted; 
        if (data.length() == fileSize) {
            hasTransferCompleted = true; 
        } else {
            hasTransferCompleted = false; 
        }

        removeFile(filename);
        addFile(filename, data, fileSize, hasTransferCompleted);
    }


    public FileInfoResponse returnFurthestDistTransientFile() {
        FileInfoResponse maxDistFile = null; 
        int max = Collections.max(transientFileMap.values());

        for (Map.Entry<FileInfoResponse, Integer> entry : transientFileMap.entrySet()) {
            if (entry.getValue() == max) {
                maxDistFile = entry.getKey(); 
            }
        }

        return maxDistFile; 
    }

    public boolean hasEnoughStorage(String originalFileData) {
        if (getStorage() + originalFileData.length() > getMaxStorage()) {
            int i = 0; 
            while (i < transientFileMap.size()) {
                FileInfoResponse file = returnFurthestDistTransientFile(); 
                deleteFromTransientMap(file);
                deleteStorage(file.getFileSize());
                i++;
                if (getStorage() + originalFileData.length() <= getMaxStorage()) {
                    return true; 
                }
            }
            return false;
        } else {
            return true; 
        }
    }

    public void addToTransientMap(FileInfoResponse file, int dist) { 
        transientFileMap.put(file, dist);
    }

    public void deleteFromTransientMap(FileInfoResponse file) { 
        transientFileMap.remove(file);
    }

    public FileInfoResponse getTransientFile(FileInfoResponse file) { 
        String filename = file.getFilename();
        FileInfoResponse transientFile = null;
        for (Map.Entry<FileInfoResponse, Integer> entry : transientFileMap.entrySet()) {
            if (filename.equals(entry.getKey().getFilename())) {
                transientFile = entry.getKey(); 
            }
        }
        return transientFile; 
    }

    public void inRangeUpdates(FileInfoResponse file) {
        if (getTransientFile(file) != null) {
            deleteFromTransientMap(file);
            receiveFromNewObject();
        }
    }

    
    public void outOfRangeUpdates(String filename, FileInfoResponse file, Device fromDevice) {
        if (getTransientFile(file) == null) {
            stopReceivingFromObject();;
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), fromDevice.getPosition()); 
            addToTransientMap(file, dist);
        }     


    }

    public void outOfRangeUpdates(String filename, FileInfoResponse file, StorageSatellite fromSatellite) {
        if (getTransientFile(file) == null) {
            fromSatellite.stopSendingToObject();
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), fromSatellite.getHeight(), fromSatellite.getPosition()); 
            addToTransientMap(file, dist); 
        }  
    }

    public boolean isReachable(Satellite toSatellite) { 
        if (MathsHelper.isVisible(toSatellite.getHeight(), toSatellite.getPosition(), getHeight(), getPosition())) {
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), toSatellite.getHeight(), toSatellite.getPosition()); 
            if (dist <= getMaxRange()) {
                return true; 
            }
        }

        return false; 
    }

    public boolean isReachable(Device toDevice) {
        if (!(getType().equals("ElephantSatellite") && toDevice.getType().equals("HandheldDevice"))) {

            if (MathsHelper.isVisible(getHeight(), getPosition(), toDevice.getPosition())) {
                int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), toDevice.getPosition()); 
                if (dist <= getMaxRange()) {
                    return true; 
                }
            }
        }

        return false; 
    }
    
}