package unsw.blackout;

import unsw.utils.Angle;
import unsw.utils.MathsHelper;
import unsw.response.models.FileInfoResponse;

public class StandardSatellite extends StorageSatellite {
    
    private final int maxRange = 150000;
    private final double linearVelocity = 2500; 
    private final int maxStorage = 80; 
    private final int bandwidthReceive = 1; 
    private final int bandwidthSend = 1; 

    public StandardSatellite(Angle position, double height, String id) {
        super(position, height, id);
        setType("StandardSatellite");
    }

    public void angularMovement(int mins) {
        double radiansMoved = (linearVelocity/getHeight()) * mins; 
        Angle radiansShifted = Angle.fromRadians(radiansMoved); 
        Angle originalAngle = getPosition(); 
        Angle newPos = originalAngle.add(radiansShifted);
        
        if (1 == newPos.compareTo(Angle.fromDegrees(360.0))) {
            newPos = newPos.subtract(Angle.fromDegrees(360.0)); 
        } 
        
        setPosition(newPos);
    }

    public int getMaxRange() {
        return maxRange; 
    }

    public int currentBytesSendPerminute() {
        return bandwidthSend; 
    }
    
    public int currentBytesReceivePerminute() {
        return bandwidthReceive;  
    }

    
    public int maxBytesSendPerminute() {
        return bandwidthSend; 
    }

    public int maxBytesReceivePerminute() {
        return bandwidthReceive;
    }
    
    
    public int getMaxStorage() {
        return this.maxStorage; 
    }

    public void addChar(String filename, char increment) {
        FileInfoResponse file = getFile(filename);
        String data = file.getData() + increment; 
        int fileSize = file.getFileSize();
        boolean hasTransferCompleted; 
        if (data.length() == fileSize) {
            hasTransferCompleted = true; 
        } else {
            hasTransferCompleted = false; 
        }

        removeFile(filename);
        addFile(filename, data, fileSize, hasTransferCompleted);
    }

    public boolean exceedsFileLimit() {
        if (numberOfFiles() >= 3) {
            return true; 
        } else {
            return false; 
        }
    }
    
    public boolean hasEnoughStorage(String originalFileData) {
        if (getStorage() + originalFileData.length() > getMaxStorage() || (exceedsFileLimit() == true)) {
            return false;
        } else {
            return true; 
        }
    }
    
    public void inRangeUpdates(FileInfoResponse file) {

    }

    public void outOfRangeUpdates(String filename, FileInfoResponse file, Device fromDevice) { 
        deleteFromFileTransferMap(filename);
        removeFile(filename);
        stopReceivingFromObject();
        deleteStorage(file.getFileSize());
    }

    public void outOfRangeUpdates(String filename, FileInfoResponse file, StorageSatellite fromSatellite) {
        deleteFromFileTransferMap(filename);
        removeFile(filename);
        fromSatellite.stopSendingToObject();
        deleteStorage(file.getFileSize());
    }

    
    public boolean isReachable(Satellite toSatellite) { 
        if (MathsHelper.isVisible(toSatellite.getHeight(), toSatellite.getPosition(), getHeight(), getPosition())) {
            int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), toSatellite.getHeight(), toSatellite.getPosition()); 
            if (dist <= getMaxRange()) {
                return true; 
            }
        }

        return false; 
    }

    public boolean isReachable(Device toDevice) {
        if (!(getType().equals("StandardSatellite") && toDevice.getType().equals("DesktopDevice"))) {
            if (MathsHelper.isVisible(getHeight(), getPosition(), toDevice.getPosition())) {
                int dist = (int) MathsHelper.getDistance(getHeight(), getPosition(), toDevice.getPosition()); 
                if (dist <= getMaxRange()) {
                    return true; 
                }
            }
        }

        return false; 
    }
}


