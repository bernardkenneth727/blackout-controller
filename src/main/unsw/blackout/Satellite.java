package unsw.blackout;

import java.util.HashMap;
import java.util.Map;
import unsw.response.models.FileInfoResponse;
import unsw.response.models.EntityInfoResponse;
import unsw.utils.Angle;

public abstract class Satellite {
    private String type;
    private String satelliteId; 
    private double height; 
    private Angle position; 
    protected Map<String, FileInfoResponse> files = new HashMap<>();
    
    public Satellite(Angle position, double height, String id) {
        this.position = position;
        this.height = height; 
        this.satelliteId = id; 
    }

    public String getId() {
        return satelliteId; 
    }

    public Angle getPosition() {
        return this.position; 
    }

    public void setPosition(Angle newAngle) {
        this.position = newAngle; 
    }

    public String getType() {
        return this.type; 
    }

    public void setType(String type) {
        this.type = type; 
    }

    public double getHeight() {
        return this.height; 
    }

    public EntityInfoResponse getInfo(String id) {
        EntityInfoResponse info = new EntityInfoResponse(id, this.position, this.height, this.type);
        return info; 
    }

    public Map<String, FileInfoResponse> getFiles() {
        return this.files; 
    }

    public abstract void angularMovement(int mins); 

    public abstract int getMaxRange(); 

    public abstract boolean isReachable(Satellite toSatellite); 
    
    public abstract boolean isReachable(Device toDevice); 
}

