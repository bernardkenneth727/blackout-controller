package unsw.blackout;

import java.util.HashMap;
import java.util.Map;
import unsw.response.models.EntityInfoResponse;
import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;
import unsw.utils.MathsHelper;
import unsw.utils.ZippedFileUtils;
import static unsw.utils.MathsHelper.RADIUS_OF_JUPITER;

public abstract class Device {
    
    private String type; 
    private final double height = RADIUS_OF_JUPITER;
    private Angle position; 
    private Map<String, FileInfoResponse> files = new HashMap<>();
    private Map<String, String> fileTransferInProgressMap = new HashMap<>();
    private String deviceId;

    
    public Device(Angle position, String id) {
        this.position = position;
        this.deviceId = id; 
    }
    
    /** 
     * Creates a file on device 
     * @param filename - name of file
     * @param content - data in file 
     */
    public void createFile(String filename, String content) {
        int size = content.length(); 
        FileInfoResponse file = new FileInfoResponse(filename, content, size, true);
        addToFileMap(filename, file);
    }

    /**
     * Helper function to add file to file map 
     * @param filename
     * @param file
     */
    public void addToFileMap(String filename, FileInfoResponse file) { 
        files.put(filename, file);
    }
    
    /**
     * Helper function to remove file from file map 
     * @param filename
     */
    public void removeFromFileMap(String filename) {
        files.remove(filename);
    }

    /**
     * When a device is the recipient of a file transfer, does the first stage of file transfer by creating an empty file
     * @param filename
     * @param fromSatellite
     * @param content
     * @return boolean 
     */
    public boolean newFileToUpload(String filename, StorageSatellite fromSatellite, String content) {
        if (fromSatellite.numObjectsSendingTo() < fromSatellite.maxBytesSendPerminute()) {
            addToFileTransferMap(filename, fromSatellite.getId());
            
            int size = content.length(); 
            FileInfoResponse file = new FileInfoResponse(filename, "", size, false);
            addToFileMap(filename, file); 
            fromSatellite.sendToNewObject();;
            return true; 
        } else {
            return false; 
        }  
    }
    
    /**
     * Returns info as EntityInfoResponse object 
     * @param id
     * @return EntityInfoResponse with relevant details
     */
    public EntityInfoResponse getInfo(String id) {
        EntityInfoResponse info = new EntityInfoResponse(id, getPosition(), 0, getType(), getFiles());
        return info; 
    }

    /**
     * returns max range of device 
     * @return range as int 
     */
    public abstract int getMaxRange(); 

    /**
     * returns position as an Angle object 
     * @return Angle
     */
    public Angle getPosition() {
        return this.position; 
    }

    /**
     * returns height of device
     * @return height 
     */
    public double getHeight() {
        return this.height; 
    }

    /**
     * return id of device 
     * @return String id
     */
    public String getId() {
        return this.deviceId; 
    }

    /**
     * returns type of device 
     * @return String type
     */
    public String getType() { 
        return this.type; 
    }

    /**
     * Used as a helper function where the type is set when creating the subclass devices
     * @param type
     */
    public void setType(String type) { 
        this.type = type; 
    }

    /**
     * Returns a map of all the files stored on the device 
     * @return Map 
     */
    public Map<String, FileInfoResponse> getFiles() {
        return this.files; 
    }

    /**
     * Returns the file of the filename inputted. 
     * @return FileInfoResponse file 
     */
    public FileInfoResponse getFile(String filename) {
        FileInfoResponse file = null; 
        for (Map.Entry<String, FileInfoResponse> entry : files.entrySet()) {
            if (filename.equals(entry.getKey())) {
                file = entry.getValue(); 
            }
        }
        return file; 
    }
    
    /**
     * Helper function that adds one character and carries out protocols if the the file is fully trasnferred
     * @return boolean - based on whethere the file transfer is complete 
     */
    public boolean addChar(String filename, char increment) { 
        FileInfoResponse file = getFile(filename);
        String data = file.getData() + increment; 
        int fileSize = file.getFileSize();
        boolean hasTransferCompleted; 
        if (data.length() == fileSize) {
            hasTransferCompleted = true; 
            if (ZippedFileUtils.isZipped(data)) {
                data = ZippedFileUtils.unzipFile(data); 
            }
            removeFromFileMap(filename);
            FileInfoResponse newFile = new FileInfoResponse(filename, data, data.length(), hasTransferCompleted);
            addToFileMap(filename, newFile);
            return true; 
        } else {
            hasTransferCompleted = false; 
            removeFromFileMap(filename);
            FileInfoResponse newFile = new FileInfoResponse(filename, data, fileSize, hasTransferCompleted);
            addToFileMap(filename, newFile);
            return false; 
        }
    }

    public void addToFileTransferMap(String fileName, String fromId) {
        fileTransferInProgressMap.put(fileName, fromId);
    }

    public void deleteFromFileTransferMap(String fileName) {
        fileTransferInProgressMap.remove(fileName);
    }

    public Map<String, String> getFilesInProgress() {
        return fileTransferInProgressMap; 
    }

    public void updateFile(StorageSatellite srcSatellite, String fileName, String originalFileData) { 
        String currentData = null;
        for (int i = 0; i < srcSatellite.currentBytesSendPerminute(); i++) {
            currentData = getFile(fileName).getData();
            
            char dataToBeTransferred = originalFileData.charAt(currentData.length()); 
            if(addChar(fileName, dataToBeTransferred)) {
                break;
            }    
        }
        
        FileInfoResponse file = getFile(fileName); 
        if (file.hasTransferCompleted()) {
            postTransferProtocols(fileName, srcSatellite);
        }
    }

    public void postTransferProtocols(String fileName, StorageSatellite srcSatellite) { 
        deleteFromFileTransferMap(fileName);
        srcSatellite.stopSendingToObject();;  
    }

    public void outOfRangeUpdates(String filename, StorageSatellite fromSatellite) {
        deleteFromFileTransferMap(filename);
        removeFromFileMap(filename);
        fromSatellite.stopSendingToObject();;
    }

    public boolean isReachable(Satellite destSatellite) { 
        if (!(destSatellite.getType().equals("StandardSatellite") && getType().equals("DesktopDevice")) && !(destSatellite.getType().equals("ElephantSatellite") && getType().equals("HandheldDevice"))) { 
            if (MathsHelper.isVisible(destSatellite.getHeight(), destSatellite.getPosition(), getPosition())) {
                int dist = (int) MathsHelper.getDistance(destSatellite.getHeight(), destSatellite.getPosition(), getPosition()); 
                if (dist <= getMaxRange()) {
                    return true; 
                }
            }
        }
        return false; 
    }

}
