package unsw.blackout;

import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;
import unsw.utils.ZippedFileUtils;

public class LaptopDevice extends Device {
    private final int maxRange = 100000;
    
    public LaptopDevice(Angle position, String id) {
        super(position, id); 
        setType("LaptopDevice");
    }

    public int getMaxRange() {
        return maxRange; 
    }


    
}