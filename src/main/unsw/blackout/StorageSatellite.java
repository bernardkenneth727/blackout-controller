package unsw.blackout;
import java.util.HashMap;
import java.util.Map;
import unsw.response.models.FileInfoResponse;
import unsw.utils.Angle;

public abstract class StorageSatellite extends Satellite{

    private Map<String, String> fileTransferInProgressMap = new HashMap<>();
    private int numObjectsSendingTo = 0; 
    private int numObjectsReceivingFrom = 0;
    private int storage = 0; 

    public StorageSatellite(Angle position, double height, String id) {
        super(position, height, id); 
    }
    
    public abstract int maxBytesSendPerminute();
       
    public abstract int maxBytesReceivePerminute();
       
    public FileInfoResponse getFile(String filename) {
        FileInfoResponse file = null; 
        for (Map.Entry<String, FileInfoResponse> entry : files.entrySet()) {
            if (filename.equals(entry.getKey())) {
                file = entry.getValue(); 
            }
        }
        return file; 
    }

    /**
     * Creates new file when the satellite is the recipient in a new file transfer
     * @param srcId
     * @param srcSatellite
     * @param filename
     * @param content
     */
    public void newFiletoUpload(String srcId, StorageSatellite srcSatellite, String filename, String content) {
        if (srcSatellite == null) {
            receiveFromNewObject();
        } else {
            srcSatellite.sendToNewObject();
        }
        
        addToFileTransferMap(filename, srcId);
        int size = content.length(); 
        FileInfoResponse file = new FileInfoResponse(filename, "", size, false);
        files.put(filename, file);
        incrementStorage(size);
    }

    public int numObjectsSendingTo() {
        return numObjectsSendingTo; 
    }

    public int numObjectsReceivingFrom() {
        return numObjectsReceivingFrom; 
    }
    
    public void sendToNewObject() {
        numObjectsSendingTo++;
    }

    public void stopSendingToObject() {
        numObjectsSendingTo--;
    }

    public void receiveFromNewObject() {
        numObjectsReceivingFrom++; 
    }

    public void stopReceivingFromObject() {
        numObjectsReceivingFrom--;
    }

    public abstract int currentBytesSendPerminute();

    public abstract int currentBytesReceivePerminute(); 

    /**
     * Adds a character to a file currently trasnferring
     * @param filename
     * @param increment
     */
    public abstract void addChar(String filename, char increment); 
    
    public void removeFile(String filename) {
        files.remove(filename); 
    }

    public void addFile(String filename, String data, int fileSize, boolean hasTransferCompleted) {
        files.put(filename, new FileInfoResponse(filename, data, fileSize, hasTransferCompleted)); 
    }
    

    public void addToFileTransferMap(String fileName, String fromId) {
        fileTransferInProgressMap.put(fileName, fromId);
    }

    public void deleteFromFileTransferMap(String fileName) {
        fileTransferInProgressMap.remove(fileName);
    }

    public Map<String, String> getFilesInProgress() {
        return fileTransferInProgressMap; 
    }

    public int getStorage() {
        return this.storage; 
    }

    public abstract int getMaxStorage();

    public void incrementStorage(int add) {
        storage = storage + add; 
    }

    public void deleteStorage(int minus) {
        storage = storage - minus; 
    }

    public int numberOfFiles() {
        return files.size();
    }

    public abstract boolean hasEnoughStorage(String originalFileData); 

    public void postTransferProtocols(String fileName, StorageSatellite srcSatellite) { 
        if (srcSatellite == null) {
            deleteFromFileTransferMap(fileName);
            stopReceivingFromObject();;
        } else {
            deleteFromFileTransferMap(fileName);
            srcSatellite.stopSendingToObject();;
        }
    }

    public void updateFile(StorageSatellite srcSatellite, String fileName, String originalFileData) {
        String currentData = null;
        for (int i = 0; i < srcSatellite.currentBytesSendPerminute(); i++) {
            currentData = getFile(fileName).getData();

            if (!currentData.equals(originalFileData)) {
                char dataToBeTransferred = originalFileData.charAt(currentData.length()); 
                addChar(fileName, dataToBeTransferred);
            } else {
                break; 
            }
        }
        
        FileInfoResponse file = getFile(fileName); 
        if (file.hasTransferCompleted()) {
            postTransferProtocols(fileName, srcSatellite);
        }
        
    }

    public void updateFile(String fileName, String originalFileData) {
        String currentData = null;
        for (int i = 0; i < currentBytesReceivePerminute(); i++) {
            currentData = getFile(fileName).getData();

            if (!currentData.equals(originalFileData)) {
                char dataToBeTransferred = originalFileData.charAt(currentData.length()); 
                addChar(fileName, dataToBeTransferred);
            } else {
                break; 
            }
        }
        
        FileInfoResponse file = getFile(fileName); 
        if (file.hasTransferCompleted()) {
            postTransferProtocols(fileName, null);
        }
    }

    /**
     * Carries out a number of updates when the src device goes out of range
     */
    public abstract void outOfRangeUpdates(String filename, FileInfoResponse file, Device fromDevice); 

    public abstract void outOfRangeUpdates(String filename, FileInfoResponse file, StorageSatellite fromSatellite); 

    /**
     * Carries out any relevant updates when the src object is in range
     */
    public abstract void inRangeUpdates(FileInfoResponse file); 
}
